//
//  DataHandler.swift
//  LearnLanguage
//
//  Created by Muhammad Mashood Tanveer on 28/01/2018.
//  Copyright © 2018 Muhammad Mashood Tanveer. All rights reserved.
//

import Foundation
class DataHandler: NSObject {
     //Fetches json from the words.json file and returns the whole json as NSArray
    func getWordsArray() ->  NSArray{
        if let path = Bundle.main.path(forResource: "words", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as! NSArray
                return jsonResult
            } catch {
                return NSArray()
            }
        }
        return NSArray()
    }
}
