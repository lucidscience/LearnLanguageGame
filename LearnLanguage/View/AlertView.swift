//
//  AlertView.swift
//  LearnLanguage
//
//  Created by Muhammad Mashood Tanveer on 28/01/2018.
//  Copyright © 2018 Muhammad Mashood Tanveer. All rights reserved.
//

import Foundation
import UIKit

class AlertView: UIView {
    var alertMessage = ""
    var alertColor = UIColor.clear
    
    // This method set the message and color of the alert according to the alert type that is passed to the function.
    func setAlertInfo(gameStatus: GameStatus) {
        switch gameStatus {
        case GameStatus.wrongAnswer:
            alertMessage = GameStatus.wrongAnswer.rawValue
            alertColor = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 0.4)
            break
        case GameStatus.trueAnswer:
            alertMessage = GameStatus.trueAnswer.rawValue
            alertColor = UIColor(red: 0.0, green: 1.0, blue: 0.0, alpha: 0.4)
            break
        case GameStatus.questionPassed:
            alertMessage = GameStatus.questionPassed.rawValue
            alertColor = UIColor(red: 0.86, green: 0.86, blue: 0.86, alpha: 0.4)
            break
        case GameStatus.userLost:
            alertMessage = GameStatus.userLost.rawValue
            alertColor = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 0.4)
            break
        case GameStatus.stageCleared:
            alertMessage = GameStatus.stageCleared.rawValue
            alertColor = UIColor(red: 0.0, green: 1.0, blue: 0.0, alpha: 0.4)
            break
        case GameStatus.gameCompleted:
            alertMessage = GameStatus.gameCompleted.rawValue
            alertColor = UIColor(red: 0.0, green: 1.0, blue: 0.0, alpha: 0.4)
            break
        }
    }
    // This method creates a view according to the device screen width and height and return it.
    func getAlertView() -> UIView {
        let alertView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        alertView.backgroundColor = alertColor
        let messageLbl = UILabel.init(frame: CGRect(x: 0, y: (UIScreen.main.bounds.height/2 - 30), width: UIScreen.main.bounds.width, height: 60))
        messageLbl.font = UIFont.boldSystemFont(ofSize: 24)
        messageLbl.textColor = UIColor.white
        messageLbl.text = alertMessage
        messageLbl.numberOfLines = 2
        messageLbl.textAlignment = .center
        alertView.addSubview(messageLbl)
        return alertView
    }
}
