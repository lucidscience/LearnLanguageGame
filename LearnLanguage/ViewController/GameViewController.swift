//
//  GameViewController.swift
//  LearnLanguage
//
//  Created by Muhammad Mashood Tanveer on 28/01/2018.
//  Copyright © 2018 Muhammad Mashood Tanveer. All rights reserved.
//

import Foundation
import UIKit

enum GameStatus: String {
    case wrongAnswer = "Incorrect Answer!"
    case trueAnswer = "Great!"
    case questionPassed = "Passed!"
    case userLost = """
    You answered 10 questions incorrectly.
    Try again with better prepartion!
    """
    case stageCleared = """
    Wow, you completed the stage.
    Continue Rocking!
    """
    case gameCompleted = """
    Awesome, you have completed the Game like a Pro.
    Stay Hungry, Stay Learning!
    """
}

class GameViewController: UIViewController {
    @IBOutlet weak var questionLbl: UILabel!
    @IBOutlet weak var answerLbl: UILabel!
    @IBOutlet weak var scoreLbl: UILabel!
    @IBOutlet weak var trueBtn: UIButton!
    @IBOutlet weak var falseBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    var languageArray: NSArray!
    var correctAnswer: Bool!
    var noAskedQuestions: Int = 0
    var noCorrectAnswers: Int = 0
    var noPassedQuestions: Int = 0
    var dropDownAnimationTime: Float = 8.0
    var isquestionPassed = true
    
    override func viewDidLoad() {
        loadLanguageDict()
        super.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        setQuestion()
        super.viewDidAppear(false)
    }
    //True button handler
    @IBAction func trueBtn(_ sender: Any) {
        userPickedAnswerAction(userAnswer: true)
    }
    //False button handler
    @IBAction func falseBtn(_ sender: Any) {
        userPickedAnswerAction(userAnswer: false)
    }
    //To exit from Game
    @IBAction func cancelBtn(_ sender: Any) {
        self.answerLbl.layer.removeAllAnimations()
        self.dismiss(animated: false, completion: nil)
    }
    //This function call DataHandler function to get the array of all questions and answers.
    func loadLanguageDict() -> Void {
        let dataHandler: DataHandler = DataHandler()
        languageArray = dataHandler.getWordsArray()
    }
    //This function adds animation on the answer label and handle passed question scenerio by calling the checkGameStatus function.
    func startTopDownAnimation() -> Void {
        UIView.animate(withDuration: TimeInterval(dropDownAnimationTime), animations: {
            self.answerLbl.frame.origin = CGPoint(x: self.view.center.x - self.answerLbl.frame.size.width/2, y: self.view.frame.size.height)
        }, completion: {finished in
            if(self.isquestionPassed && self.viewIfLoaded?.window != nil) {
                self.checkGameStatus(isCorrectAnswer: false)
            }
        })
    }
    /*
     This function is called when user selects an answers. Afterwards, it call the checkGameStatus
     function to check the game rules and give visual feedback. Moreover, it remove animation from
     the answer label.
    */
    func userPickedAnswerAction(userAnswer: Bool) -> Void {
        isquestionPassed = false
        if(userAnswer == correctAnswer) {
            noCorrectAnswers += 1
            checkGameStatus(isCorrectAnswer: true)
        } else {
            checkGameStatus(isCorrectAnswer: false)
        }
        self.answerLbl.layer.removeAllAnimations()
        
    }
    /*
     This function set the queston in the question label, decides randomly whether to show correct
     answer or the wrong one. Moreover, it increment the number of asked questions and send request for
     drop down animation start.
     */
    func setQuestion() -> Void {
        isquestionPassed = true
        self.answerLbl.frame.origin = CGPoint(x: self.view.center.x - self.answerLbl.frame.size.width/2, y: self.answerLbl.frame.size.height)
        correctAnswer = getRandomBool()
        self.questionLbl.text = (languageArray[noAskedQuestions] as! NSDictionary)["text_eng"] as? String
        if(correctAnswer) {
            self.answerLbl.text = (languageArray[noAskedQuestions] as! NSDictionary)["text_spa"] as? String
        }else {
            self.answerLbl.text = (languageArray[languageArray.count % (noAskedQuestions == 0 ? 2 : noAskedQuestions)] as! NSDictionary)["text_spa"] as? String
        }
        self.shouldHideScreenOutlets(hide: false)
        noAskedQuestions += 1
        startTopDownAnimation()
    }
    //This functions generate random bool and returns.
    func getRandomBool() -> Bool {
        return arc4random_uniform(2) == 0 ? true : false
    }
    //This functions shake the viewcontroller view on wrong answers to give better user experience.
    func shakeView() {
        let shakeAnimation = CABasicAnimation(keyPath: "position")
        shakeAnimation.duration = 0.07
        shakeAnimation.repeatCount = 3
        shakeAnimation.autoreverses = true
        shakeAnimation.fromValue = CGPoint(x: self.view.center.x - 10, y: self.view.center.y)
        shakeAnimation.toValue = CGPoint(x: self.view.center.x + 10, y: self.view.center.y)
        self.view.layer.add(shakeAnimation, forKey: "position")
    }
    /*
     This function is called after each question (either question is answered or passed) and it further
     calls the functions to set the scoreboard, display alertview, and show next questions or exit.
     Morover this function includes the rule that after each stage(10 questions), it increase the
     difficulty level by decreasing the animation time.
    */
    func checkGameStatus(isCorrectAnswer: Bool) {
        let customAlertView: AlertView = AlertView()
        var shouldFinishGame = false
        var alertDisplayTime = 1.0
        if(isquestionPassed) {
            /*
             This condition adds the rule that if user did not answered then update passed questions
             count and give the feedback accordingly.
             */
            customAlertView.setAlertInfo(gameStatus: GameStatus.questionPassed)
            self.noPassedQuestions += 1
        } else if(noAskedQuestions == languageArray.count) {
            /*
             This condition adds the rule that if the questions are finished then finish the
             game.
             */
            customAlertView.setAlertInfo(gameStatus: GameStatus.gameCompleted)
            alertDisplayTime = 3.0
            shouldFinishGame = true
        } else if (noAskedQuestions-noCorrectAnswers-noPassedQuestions >= 10) {
            /*
             This condition adds the rule that if user answered 10 questions incorrectly then finish the
             game.
             */
            alertDisplayTime = 4.0
            customAlertView.setAlertInfo(gameStatus: GameStatus.userLost)
            shouldFinishGame = true
        } else if(noAskedQuestions % 10 == 0) {
            /*
             This condition adds the rule that after every 10 questions, if the number of incorrect
             answers were less than 10 then give feedback to the user that he has cleared the stage.
             */
            alertDisplayTime = 3.0
            customAlertView.setAlertInfo(gameStatus: GameStatus.stageCleared)
            if(dropDownAnimationTime > 2) {
                dropDownAnimationTime -= 2
            }
        } else if (isCorrectAnswer) {
            /*
             This condition adds the rule that if user answered correctly then give the
             feedback accordingly.
             */
            customAlertView.setAlertInfo(gameStatus: GameStatus.trueAnswer)
        }else {
            /*
             This condition adds the rule that if user answered incorrectly then give the
             feedback accordingly.
             */
            self.shakeView()
            customAlertView.setAlertInfo(gameStatus: GameStatus.wrongAnswer)
        }
        self.questionLbl.text = ""
        self.shouldHideScreenOutlets(hide: true)
        let alertView = customAlertView.getAlertView()
        self.view.addSubview(alertView)
        DispatchQueue.main.asyncAfter(deadline: .now() + alertDisplayTime) {
            alertView.removeFromSuperview()
            if(shouldFinishGame || self.noAskedQuestions == self.languageArray.count) {
                self.answerLbl.layer.removeAllAnimations()
                self.dismiss(animated: false, completion: nil)
            }else {
                self.setScoreBoard()
                self.setQuestion()
            }
        }
    }
    /*This method hide or display the buttons and labels from the game screen to give better view of
      alert.*/
    func shouldHideScreenOutlets(hide: Bool) -> Void {
        self.trueBtn.isHidden = hide
        self.falseBtn.isHidden = hide
        self.scoreLbl.isHidden = hide
        self.cancelBtn.isHidden = hide
    }
    //This method update the number of correct, wrong and passed answers in the score Label.
    func setScoreBoard() -> Void {
        scoreLbl.text = """
        Correct: \(noCorrectAnswers)
        Wrong: \(noAskedQuestions-noCorrectAnswers-noPassedQuestions)
        Passed: \(noPassedQuestions)
        """
    }
    deinit{
        self.answerLbl.layer.removeAllAnimations()
    }
}
